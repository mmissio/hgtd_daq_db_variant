# HGTD_DAQ_DB_variant

ATLAS QT : first draft of the design and structure for the HGTD DAQ database

## Getting started
* Clone the repository
* `cd HGTD_DAQ_DB_variant`
* `mkdir build`
* `cd build`
* `cmake ..`

## Compiling the code
* `source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-09-03-00`
* `cd build`
* `make `
* `./variant_ALTIROC`

A json file (called "test.json") is created and filled with the variant objects made in test.variant_ALTIROC.cc

