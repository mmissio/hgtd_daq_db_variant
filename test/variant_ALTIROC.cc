#include <variant.hpp>
#include <fstream>

int main(int argc,char *argv[]){

  //Create registers and their values
  int confPixReg0 = 0b00000000;
  int confPixReg1 = 0b00001111;
  int confPixReg2 = 0b00111;
  int confPixReg3 = 0b01000000;
  int confLdpuIniWin1L = 0x00;
  int confLdpuIniWin1H = 0x18;
  int confLdpuIniWin2L = 0x00;
  int confLdpuIniWin2H = 0xFF;
  int CONF_LDPU_CNTRL  = 0b0001;
  int confTdpuLatencyL = 0x79;
  int confTdpuLatencyH = 0x5;
  int CONF_TDPU_CNTRL  = 0b011010;
  int CONF_TDPU_PATTERN= 0xAA;
  int CONF_TDPU_OCUPREF= 0x1F;

  //Create variant object
  variant v;

  //Fill the variant object
  v["ALTIROC"]["3EC1FQ1PEB401"]["Global"] = {
    {"confPixReg0", confPixReg0}, {"confPixReg1", confPixReg1}, {"confPixReg2", confPixReg2}, {"confPixReg3", confPixReg3}, 
    {"confLdpuIniWin1L", confLdpuIniWin1L}, {"confLdpuIniWin1H", confLdpuIniWin1H}, {"confLdpuIniWin2L", confLdpuIniWin2L}, {"confLdpuIniWin2L", confLdpuIniWin2L},
    {"CONF_LDPU_CNTRL", CONF_LDPU_CNTRL}, {"confTdpuLatencyL", confTdpuLatencyL}, {"confTdpuLatencyH", confTdpuLatencyH},
    {"CONF_TDPU_CNTRL", CONF_TDPU_CNTRL}, {"CONF_TDPU_PATTERN", CONF_TDPU_PATTERN}
  };

  //Another way of filling the variant
  v["ALTIROC"]["3EC1FQ1PEB401"]["Global"]["CONF_TDPU_OCUPREF"] = CONF_TDPU_OCUPREF;

  //Print the content of the variant object
  v.dump();

  //Create and fill a json file with the variant
  std::ofstream myfile("test.json");
  myfile<<v;

}



